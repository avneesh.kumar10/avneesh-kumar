/** *************Angular controller JS*********************/
"use strict";
app.controller('ContactController', function ($scope, $http) {
    $scope.result = 'hidden'
    $scope.resultMessage;
    $scope.formData; //formData is an object holding the name, email, subject, and message
    $scope.submitButtonDisabled = false;
    $scope.submitted = false; //used so that form errors are shown only after the form has been submitted
    $scope.submit = function (contactform, e) {
        e.preventDefault();
        $scope.submitted = true;
        $scope.submitButtonDisabled = true;
        if (contactform.$valid) {
            $http({
                method: 'POST',
                crossDomain: true,
                url: 'https://devspace-78cb.restdb.io/mail',                
                //data: $.param($scope.formData),  //param method from jQuery
                data: JSON.stringify("{\n    \"to\":\"avneesh.kumar108@gmail.com\",\n    \"subject\":\"data transformer\", \n    \"html\": \"<p>abcdefgh </p>\", \n    \"company\": Dev Space\", \n    \"sendername\": \"Dev Space query\"\n}"),
                headers: { 'Content-Type': 'application/json', 'x-apikey': '5e85c8b7111788414066c534' }  //set the headers so angular passing info as form data (not request payload)
            }).success(function (data) {
                console.log(data);
                if (data.success) { //success comes from the return json object
                    $scope.submitButtonDisabled = false;
                    $scope.formData = null;
                    $scope.resultMessage = data.message;
                    $scope.result = 'bg-success';
                } else {
                    $scope.submitButtonDisabled = false;
                    $scope.resultMessage = data.message;
                    $scope.result = 'bg-danger';
                }
            });
        } else {
            $scope.submitButtonDisabled = false;
            $scope.resultMessage = 'Failed :( Please fill out all the fields.';
            $scope.result = 'bg-danger';
        }
    }
});